using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActionData",menuName ="Test/ActionsData")]
public class ActionData : ScriptableObject
{
    [SerializeField]
    List<ActionsData> actors;
}
[System.Serializable]
public class ActionsData
{
    [SerializeField]
    int GTIN;
    [SerializeField]
    string ProductCode;
    [SerializeField]
    string ProductID;
    [SerializeField]
    string QRcodeProduct;


}