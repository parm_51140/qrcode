using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.Networking;
using System;

public class CallToGoogleSheet : MonoBehaviour
{
    QRcodeGenerate _qRcodegen;
    private byte[] rawData;
    private string _gtinproduct;
    private string _productcode;
    private string _idproduct;
    //url google sheet
    private string URL = "https://spreadsheets.google.com/pub?hl=en&hl=en&key=";
    public string spreadsheetKey = "1Il3k1sZDwkrVHTzT5sJSsuNY8Os3cPRFjYsNYd32ats";


    IEnumerator Post(string gtin, string productcode, string idproduct)
    {
        //WWWForm(UnityWebRequest) helper to generate data to post to web servers
        WWWForm form = new WWWForm();
        //Add file with given string value
        form.AddField("GTIN", gtin);
        form.AddField("ProductCode", productcode);
        form.AddField("IDProduct", idproduct);
        var download = UnityWebRequest.Post(URL+spreadsheetKey+ "&output=csv", form);
        yield return download.SendWebRequest();

        if (download.result != UnityWebRequest.Result.Success)
        {
            Console.WriteLine("Error Downloading :" + download.error);
        }
        else
        {
            Console.WriteLine(download.downloadHandler.text);
        }
    }

    public void Send()
    {
        StartCoroutine(Post(_gtinproduct, _productcode, _idproduct));
        Debug.Log(URL + spreadsheetKey + "&output=csv");
    }
    public void ChackInputFieldText()
    {
        _qRcodegen = GameObject.Find("Canvas/QRCodeGenerator").GetComponent<QRcodeGenerate>();//instance objecd
        _gtinproduct = _qRcodegen.GTINProduct.text;
        _productcode = _qRcodegen.ProductCode.text;
        _idproduct = _qRcodegen.IDProduct.text;
    }
}
