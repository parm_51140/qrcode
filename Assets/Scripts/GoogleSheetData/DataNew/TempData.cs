using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TempData {

    //Tempdata form google sheet
    public class Tems {

        public Dictionary<string, int> languageIndicies;

        public Dictionary<string, string[]> termlransIations;

        public Tems()
        {
            languageIndicies = new Dictionary<string, int>();
            termlransIations = new Dictionary<string, string[]>();
        }
    }
}
