using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CSVDownloader
{
    //Private IDsheetkey link google sheet  
    private const string k_sheetID = "11-qU0VlDVNek9wxZmXM3mEDsB-t1NZA8h82pNx2E-7M";
    private const string url = "https://docs.google.com/spreadsheets/d/" + k_sheetID + "/export?format=csv";

    //URL google sheet
    //string s = "https://docs.google.com/spreadsheets/d/11-qU0VlDVNek9wxZmXM3mEDsB-t1NZA8h82pNx2E-7M/export?format=csv ";
    //IEnumerator is Downloaddata sheet in link url
    internal static IEnumerator DownloadData(System.Action<string> oncompleted) {
        yield return new WaitForEndOfFrame();
        string downloaedDatas = null;
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            yield return webRequest.SendWebRequest();
            //Web url error network
            if (webRequest.isNetworkError)
            {
                //Debug error web
                Debug.Log("Download Error: " + webRequest.error);
                //player prefs get string url last data down loaded
                downloaedDatas = PlayerPrefs.GetString("LastDataDownloaded", null);
                string versionText = PlayerPrefs.GetString("LastDataDownloaded", null);
                Debug.Log("using stale data versiontext: " + versionText);
            }
            else
            {
                Debug.Log("Download success");
                Debug.Log("Data: " + webRequest.downloadHandler.text);
                //First term will be preceeded by version number
                string versionSection = webRequest.downloadHandler.text.Substring(0, 5);
                //Index version Section values
                int equalsIndex = versionSection.IndexOf('-');
                //us unityengine assert false to in equalsIndex == -1 to start CVS
                UnityEngine.Assertions.Assert.IsFalse(equalsIndex == -1, "Could not find a '=' at the start of the CVS");
                string versiontext = webRequest.downloadHandler.text.Substring(0, equalsIndex);
                Debug.Log("Downloaded data version" + versiontext);
                PlayerPrefs.SetString("LastDataDownloaded", webRequest.downloadHandler.text);
                PlayerPrefs.SetString("LastDataDownloadedVersion", versiontext);
                //downloaed data to webRequest down load Handler text to index + 1
                downloaedDatas = webRequest.downloadHandler.text.Substring(equalsIndex + 1);
            }

        }
        oncompleted(downloaedDatas);

    }

}
