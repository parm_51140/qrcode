using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ReadGoogleSheets : MonoBehaviour
{
    public Text outputArea;
    private string URL = "https://docs.google.com/spreadsheets/d/";
    public string spreadsheetKey = "1Il3k1sZDwkrVHTzT5sJSsuNY8Os3cPRFjYsNYd32ats";

    private void Start()
    {
        StartCoroutine(ObtainSheetData());
    }
    IEnumerator ObtainSheetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(URL + spreadsheetKey + "&output=csv");
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log("Error: "+www.error);
            Debug.Log(www.isHttpError.ToString());
        }
        else
        {
            string updataText = "";
            string json = www.downloadHandler.text;
            var o = JSON.Parse(json);
            foreach (var item in o["feed"]["entry"])
            {
                var itemo = JSON.Parse(item.ToString());

                updataText += itemo["\nProduct Code\nGS1(91)"]["$t"] +" "+ itemo["s"]["s"]+ ":" + itemo["s"]["s"];
            }
            outputArea.text = updataText;
        }
    }
}

