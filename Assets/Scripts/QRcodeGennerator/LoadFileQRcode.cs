using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;

public class LoadFileQRcode : MonoBehaviour
{
    private byte[] bytes;
    private string LoadQRcodename;
    private int LoadQRcodenumber;
    private Texture2D QRcodeTexLoadfile;
    QRcodeGenerate _qRcodegen;
    private void Start()
    {
        _qRcodegen = GameObject.Find("Canvas/QRCodeGenerator").GetComponent<QRcodeGenerate>();//instance objecd
    }
    private void Update()
    {
        LoadQRcodename = _qRcodegen.NameQRcode;
        LoadQRcodenumber = _qRcodegen.NumberQRcode;
    }
    public void LoadFileImagesQRcode()//use to Load QRcode Images file byte in /Images/FlieSaveQRcodeImages
    {
        //use to bytes Read file in Application data path
        //get file Texture2D size 1,1
        //--------------------------------------------------------------------------------------------------------------------------------------
        bytes = System.IO.File.ReadAllBytes(Application.dataPath + "/Images/FlieSaveQRcodeImages" + LoadQRcodename + LoadQRcodenumber + "png");
        QRcodeTexLoadfile = new Texture2D(1,1);
        QRcodeTexLoadfile.LoadImage(bytes);
        //--------------------------------------------------------------------------------------------------------------------------------------
        GameObject.Find("ShownQRcode/ImageLoadQRcode").GetComponent<Renderer>().material.mainTexture = QRcodeTexLoadfile;
    }
}
