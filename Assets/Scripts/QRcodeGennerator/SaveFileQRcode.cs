using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEditor;

public class SaveFileQRcode : MonoBehaviour
{
    private byte[] bytes;
    private System.IO.FileStream fileStream;
    private System.IO.BinaryWriter binary;
    private Texture2D QRCodeMainTex;
    private QRcodeGenerate _qRcodegen;
    private string SaveQRcodename;
    private int SaveQRcodenumber;
    private void Start()
    {
        _qRcodegen = GameObject.Find("Canvas/QRCodeGenerator").GetComponent<QRcodeGenerate>();//instance object
    }
    private void Update()
    {
        SaveQRcodename = _qRcodegen.NameQRcode;
        SaveQRcodenumber = _qRcodegen.NumberQRcode;
    }
    public void SaveFileImagesQrcode()//when player click Save Button
    {
        //Use System.IO.File.WriteAllBytes and ReadAllBytes instead
        //System.IO.File.WriteAllBytes(Application.dataPath + "/Images/FlieSaveQRcodeImages/" + QRcodename + QRcodenumber + ".png", textureImages.EncodeToPNG());
        //--------------------------------------------------------------------------------------------------------------------------------------
        //use System IO and Binary to Create File Images QRcode
        //--------------------------------------------------------------------------------------------------------------------------------------
        QRCodeMainTex = _qRcodegen.outQrcode;
        //Conver Texture2D to Png file add to array in byte
        bytes = QRCodeMainTex.EncodeToPNG();
        //search File in project
        fileStream = new FileStream(Application.dataPath + "/Images/FlieSaveQRcodeImages/" + SaveQRcodename + SaveQRcodenumber + ".png", FileMode.Create);
        //use to binary write bytes file 
        binary = new BinaryWriter(fileStream);
        //Write File images QRcode in project
        binary.Write(bytes);
        fileStream.Close();
        _qRcodegen.NumberQRcode++;
        Debug.Log(SaveQRcodenumber);
        //--------------------------------------------------------------------------------------------------------------------------------------
    }
}
