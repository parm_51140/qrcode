using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QRCoder.Unity;
using QRCoder;
using UnityEngine.UI;
using System.IO;
using static QRCoder.PayloadGenerator;
using System.Drawing;
using System.Drawing.Drawing2D;

public class QRcodeGenerate : MonoBehaviour
{
    [HideInInspector] public InputField GTINProduct;
    [HideInInspector] public InputField ProductCode;
    [HideInInspector] public InputField IDProduct;
    private string _gtin;
    private string _productcode;
    private string _idproduct;

    private Button ResetQRcodeButton;
    private string TextALLProduct;
    //debug value QRcode
    private int GenerateNumberQRcode = 0;
    [HideInInspector] public Texture2D outQrcode;
    //use to Save File Images Name/value
    [HideInInspector] public string NameQRcode = "QRcode";
    [HideInInspector] public int NumberQRcode = 0;
    //use readonly GenerateGS1 to string in Value Qrcode
    private static readonly string[] GenerateGS1 = new string[]
    {
        "(01)","(91)","(92)","(93)","(94)","(95)","(96)","(97)","(98)",
        "(3300)","(3310)","(3350)","(3320)"
    };
    //use readonly Generate name QRcode
    private void Start()
    {
        CheckInputGameObject();
        _gtin = GTINProduct.text;
        _productcode = ProductCode.text;
        _idproduct = IDProduct.text;
    }
    private void Update()
    {
        ResetQRcodeButton = GameObject.Find("Canvas/QRCodeGenerator/ResetQRcodeButton").GetComponent<Button>();
        ResetQRcodeButton.onClick.AddListener(ResetInputField);
    }
    public void ValuesQRcodeGender()//Function Generate QRcode
    {
        //use to string get input textfield to gender Value QRcode
        TextALLProduct = GenerateGS1[0] + GTINProduct.text + GenerateGS1[1] + ProductCode.text + GenerateGS1[2] + IDProduct.text;
        //call use QRcodeGenerator to Gen QRcode
        QRCodeGenerator qrGenerator = new QRCodeGenerator();
        //Set Data QRcode to CreateQRCode
        QRCodeData qrCodeData = qrGenerator.CreateQrCode(TextALLProduct, QRCodeGenerator.ECCLevel.H, true, false);
        UnityQRCode qrCode = new UnityQRCode(qrCodeData);
        Texture2D qrCodeMainTexture2D = qrCode.GetGraphic(20);
        RenderCube(qrCodeMainTexture2D);
        GenerateNumberQRcode++;
        Debug.Log(GenerateNumberQRcode);
        outQrcode = qrCodeMainTexture2D;
    }
    public void ResetInputField()
    {
        //resetInputField
        GTINProduct.text = null;
        ProductCode.text = null;
        IDProduct.text = null;
    }
    public void RenderCube(Texture2D TexQRcode)//Render Material in Cube
    {
        GameObject.Find("ShownQRcode/QRcodeImages").GetComponent<Renderer>().material.mainTexture = TexQRcode;
    }
    public void CheckInputGameObject()//Find to All InputField in Canvas
    {
        GTINProduct = GameObject.Find("Canvas/InputFieldGroup/InputField GTIN").GetComponent<InputField>();
        ProductCode = GameObject.Find("Canvas/InputFieldGroup/InputField Product Code").GetComponent<InputField>();
        IDProduct = GameObject.Find("Canvas/InputFieldGroup/InputField IDproduet").GetComponent<InputField>();
    }
}
